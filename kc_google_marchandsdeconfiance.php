<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Kc_google_marchandsdeconfiance extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'kc_google_marchandsdeconfiance';
        $this->tab = 'market_place';
        $this->version = '1.0.0';
        $this->author = 'Kelcible';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Google Marchands de Confiance');
        $this->description = $this->l('Mise en place du badge et du module de confirmation de commande');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('KC_GOOGLE_MARCHANDSDECONFIANCE_STORE_ID', '');
        Configuration::updateValue('KC_GOOGLE_MARCHANDSDECONFIANCE_ITEM_GOOGLE_SHOPPING_ACCOUNT_ID', '');
        Configuration::updateValue('KC_GOOGLE_MARCHANDSDECONFIANCE_ORDER_EST_SHIP_DATE', '3');
        Configuration::updateValue('KC_GOOGLE_MARCHANDSDECONFIANCE_ORDER_EST_DELIVERY_DATE', '7');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayFooter') &&
            $this->registerHook('displayOrderConfirmation');
    }

    public function uninstall()
    {
        Configuration::deleteByName('KC_GOOGLE_MARCHANDSDECONFIANCE_STORE_ID');
        Configuration::deleteByName('KC_GOOGLE_MARCHANDSDECONFIANCE_ITEM_GOOGLE_SHOPPING_ACCOUNT_ID');        
        Configuration::deleteByName('KC_GOOGLE_MARCHANDSDECONFIANCE_ORDER_EST_SHIP_DATE');
        Configuration::deleteByName('KC_GOOGLE_MARCHANDSDECONFIANCE_ORDER_EST_DELIVERY_DATE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitKc_google_marchandsdeconfianceModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitKc_google_marchandsdeconfianceModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('votre identifiant Google Marchands de confiance.'),
                        'name' => 'KC_GOOGLE_MARCHANDSDECONFIANCE_STORE_ID',
                        'label' => $this->l('Identifiant'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Cette valeur doit correspondre au numéro de compte que vous utilisez pour soumettre votre flux de données de produit à Google Shopping via Google Merchant Center. Si vous disposez d\'un compte CM, utilisez le numéro du sous-compte associé à ce flux de produits'),
                        'name' => 'KC_GOOGLE_MARCHANDSDECONFIANCE_ITEM_GOOGLE_SHOPPING_ACCOUNT_ID',
                        'label' => $this->l('Numéro de compte Google Merchant Center'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Délai moyen en jours avant envoi. Le champ "ORDER_EST_SHIP_DATE" est calculé à partir de la date du jour à laquelle est ajouté ce délai.'),
                        'name' => 'KC_GOOGLE_MARCHANDSDECONFIANCE_ORDER_EST_SHIP_DATE',
                        'label' => $this->l('Délai moyen en jours avant envoi'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Délai moyen en jours avant livraison. Le champ "ORDER_EST_DELIVERY_DATE" est calculé à partir de la date du jour à laquelle est ajouté ce délai.'),
                        'name' => 'KC_GOOGLE_MARCHANDSDECONFIANCE_ORDER_EST_DELIVERY_DATE',
                        'label' => $this->l('Délai moyen en jours avant livraison'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'KC_GOOGLE_MARCHANDSDECONFIANCE_STORE_ID' => Configuration::get('KC_GOOGLE_MARCHANDSDECONFIANCE_STORE_ID', ''),
            'KC_GOOGLE_MARCHANDSDECONFIANCE_ITEM_GOOGLE_SHOPPING_ACCOUNT_ID' => Configuration::get('KC_GOOGLE_MARCHANDSDECONFIANCE_ITEM_GOOGLE_SHOPPING_ACCOUNT_ID', ''),
            'KC_GOOGLE_MARCHANDSDECONFIANCE_ORDER_EST_SHIP_DATE' => Configuration::get('KC_GOOGLE_MARCHANDSDECONFIANCE_ORDER_EST_SHIP_DATE', '3'),
            'KC_GOOGLE_MARCHANDSDECONFIANCE_ORDER_EST_DELIVERY_DATE' => Configuration::get('KC_GOOGLE_MARCHANDSDECONFIANCE_ORDER_EST_DELIVERY_DATE', '7'),
        );
        
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
//            $this->context->controller->addJS($this->_path.'views/js/back.js');
//            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
//        $this->context->controller->addJS($this->_path.'/views/js/front.js');
//        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    /*
<!-- BEGIN: Google Marchands de confiance -->
<script type="text/javascript">
  var gts = gts || [];

  gts.push(["id", "647706"]);
  gts.push(["badge_position", "BOTTOM_RIGHT"]);
  gts.push(["locale", "PAGE_LANGUAGE"]);
  gts.push(["google_base_offer_id", "ITEM_GOOGLE_SHOPPING_ID"]);
  gts.push(["google_base_subaccount_id", "ITEM_GOOGLE_SHOPPING_ACCOUNT_ID"]);

  (function() {
    var gts = document.createElement("script");
    gts.type = "text/javascript";
    gts.async = true;
    gts.src = "https://www.googlecommerce.com/trustedstores/api/js";
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(gts, s);
  })();
</script>
<!-- END: Google Marchands de confiance -->
    */
    public function hookDisplayFooter($params)
    {
        $html_return = '';
        
        $id_lang = $this->context->language->id;
        $objLang = new Language($id_lang);
        $arrlang = explode('-', $objLang->language_code);
        $PAGE_LANGUAGE = $arrlang[0].'_'.strtoupper($arrlang[1]);
        $STORE_ID = Configuration::get('KC_GOOGLE_MARCHANDSDECONFIANCE_STORE_ID', '');
        $ITEM_GOOGLE_SHOPPING_ID = '';
        $ITEM_GOOGLE_SHOPPING_ACCOUNT_ID = Configuration::get('KC_GOOGLE_MARCHANDSDECONFIANCE_ITEM_GOOGLE_SHOPPING_ACCOUNT_ID', '');
        
        
        //$html_return .= '<pre>'.print_r($params, true).'</pre>';
        $html_return .= '<!-- BEGIN: Google Marchands de confiance -->
<script type="text/javascript">
  var gts = gts || [];

  gts.push(["id", "'.$STORE_ID.'"]);
  gts.push(["badge_position", "BOTTOM_RIGHT"]);
  gts.push(["locale", "'.$PAGE_LANGUAGE.'"]);
  gts.push(["google_base_offer_id", "'.$ITEM_GOOGLE_SHOPPING_ID.'"]);
  gts.push(["google_base_subaccount_id", "'.$ITEM_GOOGLE_SHOPPING_ACCOUNT_ID.'"]);

  (function() {
    var gts = document.createElement("script");
    gts.type = "text/javascript";
    gts.async = true;
    gts.src = "https://www.googlecommerce.com/trustedstores/api/js";
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(gts, s);
  })();
</script>
<!-- END: Google Marchands de confiance -->';
        
        return $html_return;
    }

    /*
<!-- START Google Marchands de confiance Order -->
<div id="gts-order" style="display:none;" translate="no">

  <!-- start order and merchant information -->
  <span id="gts-o-id">MERCHANT_ORDER_ID</span>
  <span id="gts-o-email">CUSTOMER_EMAIL</span>
  <span id="gts-o-country">CUSTOMER_COUNTRY</span>
  <span id="gts-o-currency">CURRENCY</span>
  <span id="gts-o-total">ORDER_TOTAL</span>
  <span id="gts-o-discounts">ORDER_DISCOUNTS</span>
  <span id="gts-o-shipping-total">ORDER_SHIPPING</span>
  <span id="gts-o-tax-total">ORDER_TAX</span>
  <span id="gts-o-est-ship-date">ORDER_EST_SHIP_DATE</span>
  <span id="gts-o-est-delivery-date">ORDER_EST_DELIVERY_DATE</span>
  <span id="gts-o-has-preorder">HAS_BACKORDER_PREORDER</span>
  <span id="gts-o-has-digital">HAS_DIGITAL_GOODS</span>
  <!-- end order and merchant information -->

  <!-- start repeated item specific information -->
  <!-- item example: this area repeated for each item in the order -->
  <span class="gts-item">
    <span class="gts-i-name">ITEM_NAME</span>
    <span class="gts-i-price">ITEM_PRICE</span>
    <span class="gts-i-quantity">ITEM_QUANTITY</span>
    <span class="gts-i-prodsearch-id">ITEM_GOOGLE_SHOPPING_ID</span>
    <span class="gts-i-prodsearch-store-id">ITEM_GOOGLE_SHOPPING_ACCOUNT_ID</span>
  </span>
  <!-- end item 1 example -->
  <!-- end repeated item specific information -->

</div>
<!-- END Google Marchands de confiance Order -->    
    */
    public function hookDisplayOrderConfirmation($params)
    {
        $html_return = '';
        //$html_return .= '<pre>'.print_r($params, true).'</pre>';
        
        $currencyObj = $params['currencyObj'];
        $orderObj = $params['objOrder'];
        
        $customer = new Customer($orderObj->id_customer);
        $address_delivery = new Address($orderObj->id_address_delivery);
        $country = new Country($address_delivery->id_country);
        
        $MERCHANT_ORDER_ID = $orderObj->id;
        $MERCHANT_ORDER_DOMAIN = str_replace('http://', '', str_replace('https://', '', _PS_BASE_URL_));
        $CUSTOMER_EMAIL = $customer->email;
        $CUSTOMER_COUNTRY = $country->iso_code;
        $CURRENCY = $currencyObj->iso_code;
        $ORDER_TOTAL = Tools::ps_round($params['total_to_pay'],2);
        $ORDER_DISCOUNTS = '-'.Tools::ps_round($orderObj->total_discounts,2);
        $ORDER_SHIPPING = Tools::ps_round($orderObj->total_shipping,2);
        $ORDER_TAX = Tools::ps_round($orderObj->total_paid_tax_incl - $orderObj->total_paid_tax_excl,2);
        $ORDER_EST_SHIP_DATE = date('Y-m-d', strtotime('+'.Configuration::get('KC_GOOGLE_MARCHANDSDECONFIANCE_ORDER_EST_SHIP_DATE', '3').' days'));
        $ORDER_EST_DELIVERY_DATE = date('Y-m-d', strtotime('+'.Configuration::get('KC_GOOGLE_MARCHANDSDECONFIANCE_ORDER_EST_DELIVERY_DATE', '7').' days'));
        $HAS_BACKORDER_PREORDER = 'N';
        $HAS_DIGITAL_GOODS = 'N';
        
        $html_return .= '<!-- START Google Marchands de confiance Order -->
<div id="gts-order" style="display:none;" translate="no">';

        $html_return .= '<!-- start order and merchant information -->
  <span id="gts-o-id">'.$MERCHANT_ORDER_ID.'</span>
  <span id="gts-o-domain">'.$MERCHANT_ORDER_DOMAIN.'</span>
  <span id="gts-o-email">'.$CUSTOMER_EMAIL.'</span>
  <span id="gts-o-country">'.$CUSTOMER_COUNTRY.'</span>
  <span id="gts-o-currency">'.$CURRENCY.'</span>
  <span id="gts-o-total">'.$ORDER_TOTAL.'</span>
  <span id="gts-o-discounts">'.$ORDER_DISCOUNTS.'</span>
  <span id="gts-o-shipping-total">'.$ORDER_SHIPPING.'</span>
  <span id="gts-o-tax-total">'.$ORDER_TAX.'</span>
  <span id="gts-o-est-ship-date">'.$ORDER_EST_SHIP_DATE.'</span>
  <span id="gts-o-est-delivery-date">'.$ORDER_EST_DELIVERY_DATE.'</span>
  <span id="gts-o-has-preorder">'.$HAS_BACKORDER_PREORDER.'</span>
  <span id="gts-o-has-digital">'.$HAS_DIGITAL_GOODS.'</span>
  <!-- end order and merchant information -->';

        $html_return .= '<!-- start repeated item specific information -->';
        
        foreach($orderObj->getProducts() as $key => $detail){
            
            //$html_return .= '<pre>'.print_r($detail, true).'</pre>';
            
            $ITEM_NAME = $detail['product_name'];
            $ITEM_PRICE = Tools::ps_round($detail['price'],2);;
            $ITEM_QUANTITY = $detail['product_quantity'];
            $ITEM_GOOGLE_SHOPPING_ID = '';
            $ITEM_GOOGLE_SHOPPING_ACCOUNT_ID = Configuration::get('KC_GOOGLE_MARCHANDSDECONFIANCE_ITEM_GOOGLE_SHOPPING_ACCOUNT_ID', '');
            
            $html_return .= '
<span class="gts-item">
    <span class="gts-i-name">'.$ITEM_NAME.'</span>
    <span class="gts-i-price">'.$ITEM_PRICE.'</span>
    <span class="gts-i-quantity">'.$ITEM_QUANTITY.'</span>
    <span class="gts-i-prodsearch-id">'.$ITEM_GOOGLE_SHOPPING_ID.'</span>
    <span class="gts-i-prodsearch-store-id">'.$ITEM_GOOGLE_SHOPPING_ACCOUNT_ID.'</span>
</span>';
            
        }
  
        $html_return .= '<!-- end repeated item specific information -->';

        $html_return .= '</div>
<!-- END Google Marchands de confiance Order -->';
        
        return $html_return;
    }

}
