{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
	<h3><i class="icon icon-credit-card"></i> {l s='Google Marchands de Confiance' mod='kc_google_marchandsdeconfiance'}</h3>
	<a href="http://www.kelcible.fr" style='float:right;'>
		<img src='http://www.kelcible.fr/wp-content/themes/mastertheme/img/logo.png' alt="Kelcible, agence webmarketing">
	</a>
	
	<p>
		<strong>{l s='Devenez marchand de confiance avec Google et Kelcible !' mod='kc_google_marchandsdeconfiance'}</strong><br />
		{l s='Merci d\'avoir installer ce module.' mod='kc_google_marchandsdeconfiance'}<br />
		{l s='Les seuls paramètres à renseigner sont dans le formulaire ci-dessous.' mod='kc_google_marchandsdeconfiance'}
	</p>

</div>