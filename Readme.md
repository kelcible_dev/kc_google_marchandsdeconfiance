# Google Marchands de Confiance #

Module **Prestashop** pour la mise en place de **Google Marchands de Confiance**.

## Pourquoi ? ##
Ce module a pour objectif de permettre facilement l'intégration du badge Google marchands de confiance et l'intégration du code du module de confirmation de la commande sans devoir toucher au code du CMS Prestashop.

![Google-marchand-confiance-logo-head.png](https://bitbucket.org/repo/gzMAzE/images/596429373-Google-marchand-confiance-logo-head.png)

## Par qui ? ##
Ce module est développé par [Kelcible](http://kelcible.fr), société angevine spécialisée en référencement et webmarketing, ainsi qu'en développement de site web vitrine, boutique en ligne et développements spécifiques.

[![logo.png](https://bitbucket.org/repo/gzMAzE/images/1337052178-logo.png)](http://kelcible.fr)

## Pour qui ? ##
Toutes les boutiques en ligne utilisant le CMS Prestashop ayant besoin d'installer et configurer le badge et le module de confirmation de commande pour devenir un marchand de confiance.

![logo.png](https://bitbucket.org/repo/gzMAzE/images/2030865788-logo.png)

## Comment ? ##
Il vous suffit d'installer le module et de renseigner les paramètres du formulaire de configuration.

![Capture.PNG](https://bitbucket.org/repo/gzMAzE/images/3915530455-Capture.PNG)


## Sources ##
### Intégration du code du badge ###
https://support.google.com/trustedstoresmerchant/answer/6063080
### Intégration du code du module de confirmation de commande ###
https://support.google.com/trustedstoresmerchant/answer/6063087